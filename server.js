const express = require('express')
const app = express()

// EJS
app.set('views', './views')
app.set('view engine', 'ejs')

let message = "message from server.js"

// heure
function refreshDate(){
    // setInterval(showDate(), t)
    setInterval(() => {
        var now = new Date();
        console.log(now.toJSON())}, 1000)
    // setInterval(() => showDate(), 1000) // rafraîchissement en millisecondes
}

function showDate() {
    let date = new Date()
    let h = date.getHours();
    let m = date.getMinutes();
    let s = date.getSeconds();
    if( h < 10 ){ h = '0' + h; }
    if( m < 10 ){ m = '0' + m; }
    if( s < 10 ){ s = '0' + s; }
    let time = h + ':' + m + ':' + s
    // document.getElementById('horloge').innerHTML = time;
    return time
}


//  rendu
app.get('/', (req, res) => {
    // res.send('hello world')
    res.render('index', {message: message, test: refreshDate()})
})

// port
app.listen(3000)